
package webbrowser;

import com.sun.javafx.robot.impl.FXRobotHelper;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
/**
 *
 * @author Rizki Wahyu
 */
public class WebBrowser extends Application {
     public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
       WebView myView = new WebView();
       WebEngine myEngine =  myView.getEngine();
       myEngine.load("https://web.telegram.org/");
//       Button btn_Google = new Button("Click to Google");
//       btn_Google.setOnAction(new EventHandler<ActionEvent>(){
//       @Override
//       public void handle (ActionEvent event){
//         myEngine.load("https://www.google.co.id/");  
//       }
//       });
       VBox root = new VBox();
       root.getChildren().addAll (myView);
       Scene scene = new Scene(root, 1200, 500);
       primaryStage.setScene(scene);
       primaryStage.show();
    }
    /**
     * @param args the command line arguments
     */ 
}
